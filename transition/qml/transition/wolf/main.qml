import QtQuick 2.0
import QtQuick.Particles 2.0
Item {
    id:load
    width: 360
    height: 480
    //定义矩形区域表示天空
    Rectangle {
        id:sky
        width:parent.width
        height:parent.height-150//留150的区域来显示大地
        gradient: Gradient {//色调的梯度显示
            GradientStop {//颜色的梯度位置
                position: 0.0
                SequentialAnimation on color {//颜色的变化
                    loops: Animation.Infinite//一直变化
                    ColorAnimation { from: "DeepSkyBlue"; to: "#0E1533"; duration: 10000 }
                    ColorAnimation { from: "#0E1533"; to: "DeepSkyBlue"; duration: 10000 }
                }
            }
            GradientStop {
                position: 1.0
                SequentialAnimation on color {
                    loops: Animation.Infinite
                    ColorAnimation { from: "SkyBlue"; to: "#437284"; duration: 10000 }
                    ColorAnimation { from: "#437284"; to: "SkyBlue"; duration: 10000 }
                }
            }
        }
    }
    Item {
        width: parent.width
        height: 2 * parent.height//上下两层,为了更好的显示
        //无限360度旋转坐标
        NumberAnimation on rotation { from: 0; to: 360; duration: 20000; loops: Animation.Infinite }
        //太阳图片
        Image {
            source: "images/sun.png"; y: 10; anchors.horizontalCenter: parent.horizontalCenter
            rotation: -3 * parent.rotation
        }
        //月亮图片
        Image {
            source: "images/moon.png"; y: parent.height - 74; anchors.horizontalCenter: parent.horizontalCenter
            rotation: -parent.rotation
        }
        //粒子系统,显示星星信息
        ParticleSystem {
            id: particlesystem
            x: 0; y: parent.height/2
            width: parent.width; height: parent.height/2
            ImageParticle {
                source: "images/star.png"
                groups: ["star"]
                color: "#00333333"
                SequentialAnimation on opacity {
                    loops: Animation.Infinite
                    NumberAnimation { from: 0; to: 1; duration: 10000 }
                    NumberAnimation { from: 1; to: 0; duration: 10000 }
                }
            }
            Emitter {
                group: "star"
                anchors.fill: parent
                emitRate: parent.width / 50
                lifeSpan: 5000
            }
        }
    }
    //大地的颜色改变
    Rectangle {
        width:parent.width
        height:150
        anchors.top: sky.bottom
        gradient: Gradient {
            GradientStop {
                position: 0.0
                SequentialAnimation on color {
                    loops: Animation.Infinite
                    ColorAnimation { from: "ForestGreen"; to: "#001600"; duration: 10000 }
                    ColorAnimation { from: "#001600"; to: "ForestGreen"; duration: 10000 }
                }
            }
            GradientStop { position: 1.0; color: "DarkGreen" }
        }
        //大地上的奔狼图片
        AnimatedSprite {
            id: sprite
            anchors.bottom: parent.top
            width: 180//显示的大小
            height: 90
            source: "images/wolf.png"//图片源
            frameCount: 8//资源数量
            frameSync: false//不按时间而按帧显示
            frameWidth: 152//截取资源大小
            frameHeight: 78
            frameDuration:100//一张图片持续时间
            running: true
            loops: 50000
        }
        //x轴的变化
        SequentialAnimation on x {
            loops: Animation.Infinite
            NumberAnimation { target: sprite; property: "x";from:360; to:-150; duration: 4000; }
        }
        //文字区域的背景
        Rectangle {
            color: Qt.rgba(1, 1, 1, 0.7)
            radius: 10
            border.width: 1
            border.color: "white"
            anchors.fill: label
            anchors.margins: -10
        }
        //文字
        Text {
            id: label
            color: "#BC76CD"
            wrapMode: Text.WordWrap
            font.pixelSize: 20
            text: "我日以继夜的奔跑,只为追寻更为美好的明天!<br>"
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.leftMargin: 70
            anchors.topMargin: 50
            anchors.rightMargin: 20
            anchors.bottomMargin:20
            styleColor: "#770000"; style: Text.Sunken
            Text {
                color: "#7F6F6F"
                wrapMode: Text.WordWrap
                font.pixelSize: 19
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.bottomMargin: 10
                text: "--追梦流云"
                styleColor: "#BC76CD"; style: Text.Sunken
                SequentialAnimation on color {
                    loops: Animation.Infinite
                    ColorAnimation { from: "#BC76CD"; to: "#0E1533"; duration: 1000 }
                    ColorAnimation { from: "#0E1533"; to: "#7F6F6F"; duration: 1000 }
                }
            }
        }
    }
}
