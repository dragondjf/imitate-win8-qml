import QtQuick 2.0
Rectangle {
    id:root
    width: parent.width
    height: parent.height
    color: "transparent"
    Column {
        id: column
        anchors.centerIn: parent
        spacing: 10
        x: 10
        Behavior on opacity {
            NumberAnimation { duration: 200; easing.type: Easing.InQuint }
        }
        transform: [
            Scale {
                id: scaleTransform
                origin.x: width / 4.0
                origin.y: height / 4.0
                xScale: 1.0
                yScale: 1.0
                Behavior on xScale {
                    NumberAnimation { duration: 200; easing.type: Easing.InQuint }
                }
                Behavior on yScale {
                    NumberAnimation { duration: 200; easing.type: Easing.InQuint }
                }
            }
        ]
        Repeater {
            model: 5
            delegate: BlockButton {
                        width: root.width-20
                        border.color: "#0075f0"
                        border.width: 1
                        onClicked: {
                            scaleTransform.xScale = 0.5;
                            scaleTransform.yScale = 0.5;
                            column.opacity = 0;
                            loadingAction.start();
                        }
                        Component.onCompleted: {
                            var colors = [ "#5736B1", "#0085CF", "#00A600", "#D9522C", "#B61C45" ];
                            color = colors[index % 5];
                        }
                    }
                }
    }
    Loader {
        id: loader
        anchors.fill: parent
        SequentialAnimation {
            id: loadingAction;
            PauseAnimation { duration: 300 }
            ScriptAction {
                script: {
                    loader.source = "AppPage.qml";
                }
            }
        }
    }


}
